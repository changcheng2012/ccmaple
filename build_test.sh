#!/bin/bash

set -e
source build/envsetup.sh

opt=$1
if [[ $opt == "" ]]; then
	opt="O2"
fi

make
if [[ $? != 0 ]]; then
	exit 1
fi
make irbuild
if [[ $? != 0 ]]; then
	exit 1
fi
make mplfe
if [[ $? != 0 ]]; then
	exit 1
fi

make libcore OPT=${opt}
if [[ $? != 0 ]]; then
	exit 1
fi

dir_lists="exceptiontest helloworld iteratorandtemplate polymorphismtest rccycletest threadtest"
for dir in $dir_lists
do
	cd $MAPLE_ROOT/samples/$dir
	make OPT=O0
	if [[ $? != 0 ]]; then
		exit 1
	fi
	make clean
	make OPT=O2
	if [[ $? != 0 ]]; then
		exit 1
	fi
	make clean
done

#testall
cd $MAPLE_ROOT
if [[ $opt != "O2" ]]; then
	cp output/ops/host-x86_64-${opt} output/ops/host-x86_64-O2 -rf
fi
#1 case
python3 ${MAPLE_ROOT}/test/main.py -pFAIL --timeout=180 --test_cfg=${MAPLE_ROOT}/test/testsuite/ouroboros/test.cfg \
	test/testsuite/ouroboros/string_test/RT0001-rt-string-ReflectString/ReflectString.java -j 16 --retry 1 --fail_exit \
	--fail-verbose --debug --progress no_flush_progress
#all case
python3 test/main.py -pFAIL --timeout=180 --test_cfg=test/testsuite/ouroboros/test.cfg test//testsuite/ouroboros -j 16 --retry 1 --fail_exit
if [[ $? != 0 ]]; then
	exit 1
fi

cd $MAPLE_ROOT
make BUILD_TYPE=DEBUG
if [[ $? != 0 ]]; then
	exit 1
fi
make irbuild BUILD_TYPE=DEBUG
if [[ $? != 0 ]]; then
	exit 1
fi
make mplfe BUILD_TYPE=DEBUG
if [[ $? != 0 ]]; then
	exit 1
fi

for dir in $dir_lists
do
	cd $MAPLE_ROOT/samples/$dir
	make OPT=O0
	if [[ $? != 0 ]]; then
		exit 1
	fi
	make clean
	make OPT=O2
	if [[ $? != 0 ]]; then
		exit 1
	fi
	make clean
done
