#  openArkCompiler本地测试

## 环境配置

参考官方配置指导 https://gitee.com/openarkcompiler/OpenArkCompiler/blob/master/doc/cn/DevelopmentPreparation.md

因为仅进行本地测试，故无需准备文档中涉及的android-ndk，AOSP中也仅需要进行java-core.jar的配置，可以使用码云上提供的编译好的libcore的jar文件，你可以下载直接使用，下载链接`https://gitee.com/mirrors/java-core/`；

## 编译

首先编译出OpenArkCompiler及maple runtime部分

```shell
source build/envsetup.sh
make libcore
```

完成后我们可以在output/bin中看到maple可执行文件，在output/lib/O2中看到mrt的libcore-all.so，在output/ops中则是执行qemu用例的依赖环境。

## 测试

配置测试环境参考 https://gitee.com/openarkcompiler/OpenArkCompiler/tree/master/test

可以通过修改test/testsuite/ouroboros/test.cfg进行本地执行ouroboros测试套进行配置，官方配置中已经默认进行了配置，如无特殊要求，可直接运行。

```
[suffix]
java = //

[internal-var]
maple = <用例编译脚本>
run = <用运行脚本>
build_option = <编译脚本参数>
run_option = <运行脚本参数>

[description]
title = Maple Ouroboros Test
```

[suffix]中指明了测试文件的尾标和注释符号

[internal-var]中maple指明了测试框架编译的脚本路径，run指明了执行编译好的用例的脚本路径，build_option中指明了编译过程中各个命令的的参数配置，run_option中则是运行脚本的参数配置。

[description]仅用作介绍

## 运行

配置完成后，可执行测试框架运行。

执行单个用例

`python3 ${MAPLE_ROOT}/test/main.py -pFAIL --timeout=180 --test_cfg=${MAPLE_ROOT}/test/testsuite/ouroboros/test.cfg ${MAPLE_ROOT}/test/testsuite/ouroboros/string_test/RT0001-rt-string-ReflectString/ReflectString.java -j 16 --retry 1 --fail_exit --fail-verbose --debug --progress no_flush_progress`

执行全部用例

`python3 test/main.py -pFAIL --timeout=180 --test_cfg=test/testsuite/ouroboros/test.cfg test//testsuite/ouroboros -j 16 --retry 1 --fail_exit`

## 测试流程细节拆分

在此我们把测试框架中的各个执行步骤取出来逐一分析

以 ReflectString.java为例

* 使用java2d8将ReflectString.java编译成为dex

  `$MAPLE_ROOT/output/bin/java2d8  ReflectString.java`

* 使用dex2mpl将ReflectString.dex编译成ReflectString.mpl

  `$MAPLE_ROOT/output/bin/dex2mpl -mplt $MAPLE_ROOT/libjava-core/libcore-all.mplt -litprofile=$MAPLE_ROOT/src/mrt/codetricks/profile.pv/meta.list ReflectString.dex`

* 使用方舟编译器将用例编译成ReflectString.VtableImpl.s

  ` $MAPLE_ROOT/output/bin/maple ReflectString.mpl`

* 使用clang++将上一步的产物编译成ReflectString.VtableImpl.o

  `$MAPLE_ROOT/tools/clang+llvm-8.0.0-x86_64-linux-gnu-ubuntu-16.04/bin/clang++ -g3 -O2 -x assembler-with-cpp -march=armv8-a -target aarch64-linux-gnu -c ReflectString.VtableImpl.s`

* 使用clang++将编译好的.o同output/ops下的各依赖库链接一起来，生产ReflectString.so

  * maplelld.so.lds在链接过程中对.s中的section进行重排，并定义了一些符号用于链接过程
  * mrt_module_init.o用于将maplelld.so.lds定义的符号导出
  * -lcore-all -lcommon-bridge 将这两个so链接进入。libcore-all.so为我们编出的output/lib/O2/中的mrt动态库，libcommon-bridge.so为src/mrt/deplibs二进制开源的依赖库，二者均自动拷贝到output/ops/host-x86_64-O2中

  `$MAPLE_ROOT/tools/clang+llvm-8.0.0-x86_64-linux-gnu-ubuntu-16.04/bin/clang++ ReflectString.VtableImpl.o -L$MAPLE_ROOT/output/ops/host-x86_64-O2 -g3 -O2 -march=armv8-a -target aarch64-linux-gnu -fPIC -shared -o ReflectString.so $MAPLE_ROOT/output/ops/mrt_module_init.o -fuse-ld=lld -rdynamic -lcore-all -lcommon-bridge -Wl,-z,notext -Wl,-T$MAPLE_ROOT/output/ops/linker/maplelld.so.lds`

* 使用qemu-aarch64执行ReflectString.so，将会在qemu环境下使用mplsh来执行用例

  `/usr/bin/qemu-aarch64 -L /usr/aarch64-linux-gnu -E LD_LIBRARY_PATH=$MAPLE_ROOT/output/ops/third_party:$MAPLE_ROOT/output/ops/host-x86_64-O2:$MAPLE_ROOT/test/test_temp/run/RT0001-rt-string-ReflectString/RT0001-rt-string-ReflectString_ReflectString_java_test_cfg_1607068122 $MAPLE_ROOT/output/ops/mplsh -Xbootclasspath:libcore-all.so -cp $MAPLE_ROOT/test/test_temp/run/RT0001-rt-string-ReflectString/RT0001-rt-string-ReflectString_ReflectString_java_test_cfg_1607068122/ReflectString.so ReflectString`

  